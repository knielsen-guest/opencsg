Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: opencsg
Source: http://opencsg.org/
Comment:
  The glew directory shipped with opencsg is, in the relevant parts,
  identical with what is shipped as glew 1.11.0-2 in Debian. The copyright
  information is relevant only as far as shipping the orig tarball is
  concerned; for building the binary packages, the directory is ignored.
  .
  The content of the lower block of File sections is a copy of that package's
  debian/copyright file, stripped off the header and with the VIM expression
  s!Files: !Files: glew/! applied.

Files: *
Copyright: © 2002-2022, Florian Kirsch, Hasso-Plattner-Institute at the University of Potsdam, Germany
License: GPL-2+
  This library is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  .
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.
  .
  A copy of the GNU General Public License can be found in
  /usr/share/common-licenses/GPL-2.

Files: RenderTexture/*
Copyright: © 2002-2004 Mark J. Harris
License: ZLIB
  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any
  damages arising from the use of this software.
  .
  Permission is granted to anyone to use this software for any
  purpose, including commercial applications, and to alter it and
  redistribute it freely, subject to the following restrictions:
  .
  1. The origin of this software must not be misrepresented; you
     must not claim that you wrote the original software. If you use
     this software in a product, an acknowledgment in the product
     documentation would be appreciated but is not required.
  .
  2. Altered source versions must be plainly marked as such, and
     must not be misrepresented as being the original software.
  .
  3. This notice may not be removed or altered from any source
     distribution.

Files: debian/*
Copyright: © 2011-2017 Christian M. Amsüss <chrysn@fsfe.org>
           © 2019-2022 Kristian Nielsen <knielsen@knielsen-hq.org>
License: GPL-2+




Files: glew/*
Copyright: Copyright 2002-2008 Milan Ikits <milan.ikits@ieee.org>
           Copyright 2002-2008 Marcelo E. Magallon <mmagallo@debian.org>
           Copyright 2002 Lev Povalahev
License: BSD-3-clause

Files: glew/include/*
Copyright: Copyright 2002-2008 Milan Ikits <milan.ikits@ieee.org>
           Copyright 2002-2008 Marcelo E. Magallon <mmagallo@debian.org>
           Copyright 2002 Lev Povalahev
License: BSD-3-clause

Files: glew/src/*
Copyright: Copyright 2002-2008 Milan Ikits <milan.ikits@ieee.org>
           Copyright 2002-2008 Marcelo E. Magallon <mmagallo@debian.org>
           Copyright 2002 Lev Povalahev
License: BSD-3-clause

Files: glew/auto/bin/*
Copyright: Copyright 2002-2008 Milan Ikits <milan.ikits@ieee.org>
           Copyright 2002-2008 Marcelo E. Magallon <mmagallo@debian.org>
License: GPL-2+

Files: glew/auto/src/*
Copyright: Copyright 2002-2008 Milan Ikits <milan.ikits@ieee.org>
           Copyright 2002-2008 Marcelo E. Magallon <mmagallo@debian.org>
           Copyright 2002 Lev Povalahev
License: Mesa

Files: glew/auto/src/mesa_license.h
Copyright: Copyright 1999-2007 Brian Paul
           Copyright 2007 Hans de Goede (modifications needed for glew)
License: Expat

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.
 * The name of the author may be used to endorse or promote products
   derived from this software without specific prior written
   permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This file is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA
 02110-1301, USA.
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file `/usr/share/common-licenses/GPL-2'.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: Mesa
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 BRIAN PAUL BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
